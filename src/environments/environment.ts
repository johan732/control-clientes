// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore: {
    apiKey: 'AIzaSyDpT2_zKa1Y8F-mDBroeMxjye5um47Q3_8',
    authDomain: 'control-clientes-4b05f.firebaseapp.com',
    projectId: 'control-clientes-4b05f',
    storageBucket: 'control-clientes-4b05f.appspot.com',
    messagingSenderId: '845222147484',
    appId: '1:845222147484:web:59c4fe88e4fa97ade68459',
    measurementId: 'G-D1JJSR3KFR',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
